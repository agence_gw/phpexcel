<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Php74\Rector\ArrayDimFetch\CurlyToSquareBracketArrayStringRector;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->phpVersion(\Rector\Core\ValueObject\PhpVersion::PHP_70);

    $rectorConfig->paths([
        __DIR__ . '/Classes'
    ]);

    $rectorConfig->rule(CurlyToSquareBracketArrayStringRector::class);

    // define sets of rules
    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_74,
    ]);
};
